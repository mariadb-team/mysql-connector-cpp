Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mysql-connector-c++
Source: https://dev.mysql.com/downloads/connector/cpp/
Comment: This package was debianized by Rene Engelhard <rene@debian.org>
 on Tue, 25 Nov 2008 14:39:51 +0100.
 It was downloaded from http://forge.mysql.com/wiki/Connector_C++

Files: *
Copyright:
    2008-2018 Oracle and/or its affiliates.
License: GPL-2 and exception-universal-FOSS

Files: debian/*
Copyright:
    2008-2016 Rene Engelhard <rene@debian.org>
    2016-2019 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License 2 can be found in "/usr/share/common-licenses/GPL-2".

License: FLOSS-exception
 1. Definitions.
 .
  "Derivative Work" means a derivative work, as defined under applicable
  copyright law, formed entirely from the Program and one or more FOSS
  Applications.
 .
  "FOSS Application" means a free and open source software application
  distributed subject to a license listed in the section below titled "FOSS
  License List."
 .
  "FOSS Notice" means a notice placed by Oracle or MySQL in a copy of the
  MySQL Client Libraries stating that such copy of the MySQL Client
  Libraries may be distributed under Oracle's or MySQL’s FOSS (or FLOSS)
  License Exception.
 .
  "Independent Work" means portions of the Derivative Work that are not
  derived from the Program and can reasonably be considered independent and
  separate works.
 .
  "Program" means a copy of Oracle’s MySQL Client Libraries that contains a
  FOSS Notice.
 .
 2. A FOSS application developer ("you" or "your") may distribute a
 Derivative Work provided that you and the Derivative Work meet all of the
 following conditions:
 .
    a. You obey the GPL in all respects for the Program and all portions
      (including modifications) of the Program included in the Derivative
      Work (provided that this condition does not apply to Independent
      Works);
 .
    b. The Derivative Work does not include any work licensed under the GPL
       other than the Program;
 .
    c. You distribute Independent Works subject to a license listed in the
       section below titled "FOSS License List";
 .
    d. You distribute Independent Works in object code or executable form
       with the complete corresponding machine-readable source code on the
       same medium and under the same FOSS license applying to the object code
       or executable forms;
 .
    e. All works that are aggregated with the Program or the Derivative
       Work on a medium or volume of storage are not derivative works of the
       Program, Derivative Work or FOSS Application, and must reasonably be
       considered independent and separate works.
 .
 3. Oracle reserves all rights not expressly granted in these terms and
 conditions. If all of the above conditions are not met, then this FOSS
 License Exception does not apply to you or your Derivative Work.
 .
 FOSS License List
    License Name                                           Version(s)/Copyright Date
 -----------------------------------------------------------------------------------
    Release Early                                          Certified Software
    Academic Free License                                  2.0
    Apache Software License                                1.0/1.1/2.0
    Apple Public Source License                            2.0
    Artistic license                                       From Perl 5.8.0
    BSD license                                            "July 22 1999"
    Common Development and Distribution License (CDDL)     1.0
    Common Public License                                  1.0
    Eclipse Public License                                 1.0
    European Union Public License (EUPL) [1]               1.1
    GNU Affero General Public License (AGPL)               3.0
    GNU Library or "Lesser" General Public License (LGPL)  2.0/2.1/3.0
    GNU General Public License (GPL)                       3.0
    IBM Public License                                     1.0
    Jabber Open Source License                             1.0
    MIT License (As listed in file MIT-License.txt)        -
    Mozilla Public License (MPL)                           1.0/1.1
    Open Software License                                  2.0
    OpenSSL license (with original SSLeay license)         "2003" ("1998")
    PHP License                                            3.0/3.01
    Python license (CNRI Python License)                   -
    Python Software Foundation License                     2.1.1
    Sleepycat License                                      "1999"
    University of Illinois/NCSA Open Source License        -
    W3C License                                            "2001"
    X11 License                                            "2001"
    Zlib/libpng License                                    -
    Zope Public License                                    2.0
 .
 [1]: When an Independent Work is licensed under a "Compatible License"
 pursuant to the EUPL, the Compatible License rather than the EUPL is the
 applicable license for purposes of these FOSS License Exception Terms and
 Conditions.
Comment:
 Full text of the FLOSS License Exception is available from the following URL:
 http://www.mysql.com/about/legal/licensing/foss-exception.html

License: exception-universal-FOSS
 The Universal FOSS Exception, Version 1.0
 ․
 This page provides a copy of the Universal FOSS Exception, Version 1.0.
 Here is the text:
 ․
 The Universal FOSS Exception, Version 1.0
 ․
 In addition to the rights set forth in the other license(s) included in the
 distribution for this software, data, and/or documentation (collectively
 the "Software," and such licenses collectively with this additional
 permission the "Software License"), the copyright holders wish to
 facilitate interoperability with other software, data, and/or documentation
 distributed with complete corresponding source under a license that is
 OSI-approved and/or categorized by the FSF as free (collectively "Other
 FOSS").  We therefore hereby grant the following additional permission with
 respect to the use and distribution of the Software with Other FOSS, and
 the constants, function signatures, data structures and other invocation
 methods used to run or interact with each of them (as to each, such
 software's "Interfaces"):
 ․
 (i) The Software's Interfaces may, to the extent permitted by the license
 of the Other FOSS, be copied into, used and distributed in the Other FOSS
 in order to enable interoperability, without requiring a change to the
 license of the Other FOSS other than as to any Interfaces of the Software
 embedded therein.  The Software's Interfaces remain at all times under the
 Software License, including without limitation as used in the Other FOSS
 (which upon any such use also then contains a portion of the Software under
 the Software License).
 ․
 (ii) The Other FOSS's Interfaces may, to the extent permitted by the
 license of the Other FOSS, be copied into, used and distributed in the
 Software in order to enable interoperability, without requiring that such
 Interfaces be licensed under the terms of the Software License or otherwise
 altering their original terms, if this does not require any portion of the
 Software other than such Interfaces to be licensed under the terms other
 than the Software License.
 ․
 (iii) If only Interfaces and no other code is copied between the Software
 and the Other FOSS in either direction, the use and/or distribution of the
 Software with the Other FOSS shall not be deemed to require that the Other
 FOSS be licensed under the license of the Software, other than as to any
 Interfaces of the Software copied into the Other FOSS.  This includes, by
 way of example and without limitation, statically or dynamically linking
 the Software together with Other FOSS after enabling interoperability using
 the Interfaces of one or both, and distributing the resulting combination
 under different licenses for the respective portions thereof.
 ․
 For avoidance of doubt, a license which is OSI-approved or categorized by
 the FSF as free, includes, for the purpose of this permission, such
 licenses with additional permissions, and any license that has previously
 been so-approved or categorized as free, even if now deprecated or
 otherwise no longer recognized as approved or free.  Nothing in this
 additional permission grants any right to distribute any portion of the
 Software on terms other than those of the Software License or grants any
 additional permission of any kind for use or distribution of the Software
 in conjunction with software other than Other FOSS.
Comment: https://oss.oracle.com/licenses/universal-foss-exception/
